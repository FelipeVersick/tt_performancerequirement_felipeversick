﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    private int SlimeVarible1;
    private int SlimeVarible2;
    private int SlimeVarible3;

    public void Start()
    {
        SlimeVarible1 = 1;
        Debug.Log("Variable1 from Player is currently:" + SlimeVarible1);
    }

    public void Update()
    {
        SlimeVarible2 += 1;
        SlimeVarible3 -= 1;
        Debug.Log("Variable2 from Player is currently:" + SlimeVarible2);
        Debug.Log("Variable3 from Player is currently:" + SlimeVarible3);
    }
}
