﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dark_Knight : MonoBehaviour
{
    private int dark_KnightVariable1;
    private int dark_KnightVariable2;
    private int dark_KnightVariable3;

    public void Start()
    {
        dark_KnightVariable1 = 1;
        Debug.Log("Variable1 from Player is currently:" + dark_KnightVariable1);
    }

    public void Update()
    {
        dark_KnightVariable2 += 1;
        dark_KnightVariable3 -= 1;
        Debug.Log("Variable2 from Player is currently:" + dark_KnightVariable2);
        Debug.Log("Variable3 from Player is currently:" + dark_KnightVariable3);
    }
}
