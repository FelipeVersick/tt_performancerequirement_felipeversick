﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{
    private int dragonVarible1;
    private int dragonVarible2;
    private int dragonVarible3;

    public void Start()
    {
        dragonVarible1 = 1;
        Debug.Log("Variable1 from Player is currently:" + dragonVarible1);
    }

    public void Update()
    {
        dragonVarible2 += 1;
        dragonVarible3 -= 1;
        Debug.Log("Variable2 from Player is currently:" + dragonVarible2);
        Debug.Log("Variable3 from Player is currently:" + dragonVarible3);
    }
}
