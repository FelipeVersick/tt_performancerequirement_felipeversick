﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_2D : MonoBehaviour
{
    private int Item_2DVarible1;
    private int Item_2DVarible2;
    private int Item_2DVarible3;

    public void Start()
    {
        Item_2DVarible1 = 1;
        Debug.Log("Variable1 from Player is currently:" + Item_2DVarible1);
    }

    public void Update()
    {
        Item_2DVarible2 += 1;
        Item_2DVarible3 -= 1;
        Debug.Log("Variable2 from Player is currently:" + Item_2DVarible2);
        Debug.Log("Variable3 from Player is currently:" + Item_2DVarible3);
    }
}
