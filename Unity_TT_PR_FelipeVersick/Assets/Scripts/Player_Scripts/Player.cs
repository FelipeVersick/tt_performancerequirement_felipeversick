﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private int playerVariable1;
    private int playerVariable2;
    private int playerVariable3;

    public void Start()
    {
        playerVariable1 = 1;
        Debug.Log("Variable1 from Player is currently:" + playerVariable1);
    }

    public void Update()
    {
        playerVariable2 += 1;
        playerVariable3 -= 1;
        Debug.Log("Variable2 from Player is currently:" + playerVariable2);
        Debug.Log("Variable3 from Player is currently:" + playerVariable3);
    }
}
